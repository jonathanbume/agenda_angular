import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContactInsertRoutingModule } from './contact-insert-routing.module';
import { AddContactComponent } from './components/add-contact/add-contact.component';
import { MaterialModule } from 'src/app/material/material.module';


@NgModule({
  declarations: [
    AddContactComponent
  ],
  imports: [
    CommonModule,
    ContactInsertRoutingModule,
    MaterialModule
  ]
})
export class ContactInsertModule { }
