import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserLogin } from '../core/model/enteties/UserLogin/UserLogin';
import { UserService } from '../core/services/user.service';
import { Router } from '@angular/router';

interface LoginResponse {
  token: string;
  codeAdmin: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})



export class LoginComponent implements OnInit {
  form!: FormGroup;
static toke:string;
rpt:any;
  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService
  ) {
    this.form = this.formBuilder.group({
      user: ['', Validators.required],
      password: ['', Validators.required],
    });
  }


  showName() {}
  login() {
    if (this.form.valid) {
    }
  }
  UserLogin() {
    if (this.form.valid) {
      const formData = new FormData();
      formData.append('Emailusuario', this.form.controls['user'].value);
      formData.append('idpassword', this.form.controls['password'].value);
      const user: UserLogin = {
        EmailUsuario: this.form.get('user')?.value,
        idContraseña: this.form.get('password')?.value,
      };
     this.userService.UserLogin(formData).subscribe((rpt) => {
        console.log(rpt);
        if ('codeAdmin' in rpt) {
            // localStorage.setItem('token', rpt?.token);
            this.router.navigate(['/']);
        } else {
          alert('Credenciales inválidas');
        }
      });
    }
  }

  Registrar() {
    console.log('genere un evento a redireccionar a la pagina de usuario');
  }

}
