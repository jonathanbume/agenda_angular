import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ActivityInsertRoutingModule } from './activity-insert-routing.module';
import { AddActivityComponent } from './components/add-activity/add-activity.component';
import { MaterialModule } from 'src/app/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AddActivityComponent
  ],
  imports: [
    CommonModule,
    ActivityInsertRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ActivityInsertModule { }
