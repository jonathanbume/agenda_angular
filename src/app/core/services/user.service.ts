import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { User } from '../model/enteties/User.model';
import { SoUser } from '../model/so/So-user.model';

interface LoginResponse {
  token: string;
  codeAdmin: boolean;
}
@Injectable({
  providedIn: 'root'
})



export class UserService {

  constructor(private http: HttpClient) {}
    User(formData:FormData){

      return this.http.post('https://localhost:7036/User/Insert',formData);
    }
    getAll(){
      return this.http.get('https://localhost:7036/User/getall').
      pipe(map((Response) =>Response as SoUser))


    }
    UserLogin(formData:FormData): Observable<LoginResponse>{
      return this.http.post<LoginResponse>('https://localhost:7036/User/login',formData);
    }
   /* getLogin(mail: any, pass: any): Observable<any>{
      return this.http.get('https://localhost:7036/User/login' +'?Emailusuario='+ mail +'&idpassword=' + pass);
    }*/

}
