import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/core/model/enteties/User.model';
import { UserService } from 'src/app/core/services/user.service';
//import EditIcon from '@mui/icons-material/Edit';
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
})
export class UserListComponent implements OnInit {
  displayedColumns: string[] = [ 'Nombres', 'Apellidos', 'Dni','NumeroTelefonico','action'];
  dataSource!: User[];
  constructor(private userService: UserService) {}
  ngOnInit(): void {
    this.fechtData();
    
  }

  fechtData() {
    this.userService.getAll().subscribe((rpt) => {
      this.dataSource = rpt.listDtoService;
     console.log(rpt.listDtoService);
    });
  }
  editUser(idUsuario:string){

  }
}
